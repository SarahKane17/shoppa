<?php

namespace App;

use App\Notifications\BuyerResetPasswordNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Buyer extends Authenticatable
{
    //
    use Notifiable;
    protected $fillable = [
        'first_name', 'second_name', 'email', 'phone_number', 'status', 'password'
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new BuyerResetPasswordNotification($token));
    }
}
