<?php

namespace App\Mail;

use App\Odour;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @param Odour $odour
     */
    public function __construct(Odour $odour)
    {
        //
        $this->odour = $odour;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

//Within this method, you may call various methods such as from, subject, view, and attach to configure the email's presentation and delivery.
    public function build()
    {
        return $this->from('sarakane17@gmail.com')
                    ->to('kiamakenya97@gmail.com')
                    ->view('emails.orders.shipped');
    }
}
