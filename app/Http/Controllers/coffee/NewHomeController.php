<?php

namespace App\Http\Controllers\coffee;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class NewHomeController extends Controller
{
    //
    public function index(){
        return view('new_home',['name'=>'John']);
    }

    public function login(){
        return view('login');
    }

    public function user_details($name){
        $name ="John";
        return view('user_details',['name'=>$name]);
    }
}
