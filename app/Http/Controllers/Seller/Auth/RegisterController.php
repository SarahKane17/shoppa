<?php


namespace App\Http\Controllers\Seller\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class RegisterController extends Controller
{
    public function showRegistrationForm()
    {
        return view('seller.auth.register');
    }
    public function register(Request $request){
//        dd($request->all());
        Validator::make($request->all(),[
            'f_name'=>'required|string',
            's_name'=>'required|string',
            'number'=>'required',
            'email'=>'required',
            'password'=>'required|confirmed',

        ])->validate();
    }
}
