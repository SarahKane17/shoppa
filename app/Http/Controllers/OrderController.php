<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Odour;
use Illuminate\Support\Facades\Mail;


class OrderController extends Controller
{
    //
    public function send_email(Odour $odour){
        $this->odour = $odour;

        Mail::send('emails.orders.shipped',
            array(
                'name' => $odour->get('order_no'),

            ), function($message)
            {
                $message->from('sarakane17@gmail.com');
                $message->to('ianpeterkiama@gmail.com', 'Admin')->subject('Feedback');
            });

        return back()->with('success', 'Thanks for contacting us!');
    }
}
