<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;

class TestController extends Model
{
    //
    public function index() {
        $title= "babez";
        $heading= "changed heading";
        $context= [
            "page_title"=>$title,
            'heading'=>$heading
        ];
        return view('test', $context);
    }
}
