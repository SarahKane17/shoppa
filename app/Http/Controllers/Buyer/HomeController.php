<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index(){
        return view('buyer.home');
    }
    public function about(){
        $title= "Buyer | About";
        $title_array=[
          'title'=>$title
        ];
        return view('buyer.about', $title_array);
    }
}
