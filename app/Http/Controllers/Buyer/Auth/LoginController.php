<?php

namespace App\Http\Controllers\Buyer\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //
    use AuthenticatesUsers;

    protected $redirectTo = 'buyer/home';

    public function showLoginForm(){
        return view('buyer.auth.login');
    }

    public function guard()
    {
        return Auth::guard('buyer_web');
    }

    public function credentials(Request $request)
    {
        return [

            'email'=>$request->email,
            'password'=>$request->password,
            'status'=>1,
        ];
    }

}
