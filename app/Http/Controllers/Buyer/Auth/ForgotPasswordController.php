<?php

namespace App\Http\Controllers\Buyer\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;
    //
//
    public function showLinkRequestForm(){
        return view('buyer.auth.password.buyer_email');
    }
    public function broker(){
        return Password::broker('buyer');
    }
}
