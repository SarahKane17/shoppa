<?php


namespace App\Http\Controllers\Buyer\Auth;


use App\Buyer;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use function GuzzleHttp\Promise\all;


class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo ='/';

    public function showRegistrationForm()
    {
        return view('buyer.auth.register');Auth::user()->id;
    }
//    public function register(Request $request){
//////        dd($request->all());
////        Validator::make($request->all(),[
////            'f_name'=>'required|string',
////            's_name'=>'required|string',
////            'number'=>'required',
////            'email'=>'required',
////            'password'=>'required|confirmed',
////
////        ])->validate();
//        $this->validator($request->all())->validate();
//    }


    public function validator(array $data){
        return Validator::make($data,[
            'f_name'=>'required|string',
            's_name'=>'required|string',
            'number'=>'required',
            'email'=>'required',
            'password'=>'required|confirmed'
        ]);
    }
    protected function guard(){
        return Auth::guard('buyer_web');
    }

    protected function create(array $data){
        return Buyer::create([
            'first_name' => $data['f_name'],
            'second_name' => $data['s_name'],
            'email' => $data['email'],
            'phone_number'=>$data['number'],
            'password' => Hash::make($data['password']),
            'status' => 1,
        ]);
    }
}
