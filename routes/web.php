<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('home', function (){
    echo 'Hello echo<br>';
    return 'Hello programmer.';
});

//Route::get('test',function (){TestController.index} -> name('test.index'));
Route::get('test', 'TestController@index') -> name('test.index');
Route::get('/home', 'HomeController@index') -> name('home');
Route::prefix('coffee')->name('coffee.')->namespace('coffee')->group(function (){
    Route::get('/home', 'NewHomeController@index') -> name('home');
    Route::get('/login', 'NewHomeController@login') -> name('login');
    Route::get('/user/details/{name}', 'NewHomeController@user_details') -> name('user.details');
});
Route::resource('product', 'ProductController');
//Route::get('/test', '');

Route::get('buyer/home', 'Buyer\HomeController@index');
Route::get('buyer/about','Buyer\HomeController@about');

Route::get('/buyer/register', 'Buyer\Auth\RegisterController@showRegistrationForm');
Route::post('/buyer/register', 'Buyer\Auth\RegisterController@register')->name('buyer.register');
Route::get('/seller/register', 'Seller\Auth\RegisterController@showRegistrationForm');
Route::post('/seller/register', 'Seller\Auth\RegisterController@register')->name('seller.register');





Route::get('order/mail', function (){
    return view('order');
} );

Route::post('order/mail', 'OrderController@send_email')-> name('order.mail');

Route::get('admin/login', 'Admin\Auth\LoginController@index');
Route::get('buyer/login', 'Buyer\Auth\LoginController@showLoginForm');
Route::post('buyer/login','Buyer\Auth\LoginController@Login')->name('buyer.login');
Route::get('seller/login', 'Seller\Auth\LoginController@index');


//buyer password reset
Route::post('buyer/password/email','Buyer\Auth\ForgotPasswordController@sendResetLinkEmail')->name('buyer.password.email');
Route::get('buyer/password/reset','Buyer\Auth\ForgotPasswordController@showLinkRequestForm')->name('buyer.password.request');
Route::post('buyer/password/reset','Buyer\Auth\ResetPasswordController@reset');
Route::get('buyer/password/reset/{token}','Buyer\Auth\ResetPasswordController@showResetForm')->name('buyer.password.reset');
//Route::


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
