@extends('seller.layout.auth_master')

@section('title')
    Buyer | Register
    @endsection

@section('content')

        <div class="col-lg-6">
            <form action="{{route('seller.register')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                    @if($errors->has('email'))
                        <h6 class="text-danger">{{$errors->first('email')}}</h6>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">First Name</label>
                    <input type="name" class="form-control" name="f_name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="First Name">
                    @if($errors->has('f_name'))
                        <h6 class="text-danger">{{$errors->first('f_name')}}</h6>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Second Name</label>
                    <input type="name" class="form-control" name="s_name" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Second Name">
                    @if($errors->has('s_name'))
                        <h6 class="text-danger">{{$errors->first('s_name')}}</h6>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputPhoneNumber">Phone number</label>
                    <input type="phonenumber" class="form-control" name="number" id="exampleInputPassword1" placeholder="Phone number">
                    @if($errors->has('number'))
                        <h6 class="text-danger">{{$errors->first('number')}}</h6>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
                    @if($errors->has('password'))
                        <h6 class="text-danger">{{$errors->first('password')}}</h6>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Confirm Password</label>
                    <input type="password" class="form-control" name="confirm_password" id="exampleInputPassword1" placeholder="Confirm Password">
                    @if($errors->has('confirm_password'))
                        <h6 class="text-danger">{{$errors->first('password')}}</h6>
                    @endif
                </div>


                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

        </div>



    @endsection















