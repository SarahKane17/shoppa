@extends('buyer.auth.layout.auth_master')

@section('title')
    Buyer | Login
@endsection

@section('content')
<div class="container">
    <div class="col-lg-6">
        <form action="{{route('buyer.login')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                @if($errors->has('email'))
                    <h6 class="text-danger">{{$errors->first()}}</h6>
                @endif
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Password</label>
                <input type="password" class="form-control" name="password" id="exampleInputEmail1" placeholder="Password">
                @if($errors->has('password'))
                    <h6 class="text-danger">{{$errors->first()}}</h6>
                @endif
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Login</button>
            </div>
        </form>
    </div>
</div>
