<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <title>@yield('title')</title>
</head>
<body>
<div class="container">
<h3></h3>
@yield('content')
<h3></h3>
</div>
</body>
</html>

