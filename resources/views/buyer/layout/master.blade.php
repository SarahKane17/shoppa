<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>@yield('title')</title>
</head>
<body>
    <h3>This is buyer footer.</h3>
    <h3>This is the buyer page and it doesn't change.</h3>
    @yield('content')
    <h3>This is buyer footer.</h3>

</body>
</html>
